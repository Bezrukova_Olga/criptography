package jpegCompression;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import java.awt.*;
import java.awt.image.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Iterator;

/**
 * ����� � ����������� ���� �����������: �� ��������� � �������� ���������� ���� �����������,
 * ������������� ���, �������� ������� � ����� ������� ���, ��������� ����������� ���� JPEG
 */

public class JEncoder {
		private static int Quality; // ������� ������
		private static int BlocSize;
		private Image monImage;
		private int[][] inputArray;
		private int[][] bloc;
		private int[][] blocDCT;
		private int[][] bloc_DCT_Q;
		private int[][] outputArray;
		private int[][] red;
		private int[][] green;
		private int[][] blue;

    int width =320, height = 240; // ������ �����������
    private Image image;
    GrafixTools GT;
    DCT dctTrans;
    int locationwidth=100;
    int locationheight=10;
	private String fileName;

    public JEncoder(String imageName, int quality, int blocSize) {

        this.Quality=quality;
        this.BlocSize=blocSize;
        this.fileName=imageName;

    }

    public JEncoder() {
    }

    public void compressImage() {
        int i = 0;
        int j = 0;
        int a = 0;
        int b = 0;
        int x = 0;
        int y = 0;
        int counter = 0;

        int temp1 = 0, temp2 = 0, count = 0;

        int xpos;
        int ypos;
        //int width = width, height = height;

        float dctArray1[][] = new float[BlocSize][BlocSize];
        float[][] dctArray2 = new float[BlocSize][BlocSize];
        double[][] dctArray3 = new double[BlocSize][BlocSize];
        double[][] dctArray4 = new double[BlocSize][BlocSize];

        int reconstImage[][] = new int[width][height];
        System.out.println("Initializing compression - DCT & Qualitization");

        for (i = 0; i < width / BlocSize; i++) {
            for (j = 0; j < height / BlocSize; j++) {
                xpos = i * BlocSize;
                ypos = j * BlocSize;
                for (a = 0; a < BlocSize; a++) {
                    for (b = 0; b < BlocSize; b++) {
                        dctArray1[a][b] = (float) outputArray[xpos + a][ypos + b];
                    }
                }

                dctArray2 = dctTrans.fast_fdct(dctArray1);

                dctArray3 = CompressionTools.quantitizeBloc(dctArray2, Quality, BlocSize);

                for (a = 0; a < BlocSize; a++) {
                    for (b = 0; b < BlocSize; b++) {
                        reconstImage[xpos + a][ypos + b] = (int) dctArray2[a][b];
                    }
                }

            }
        }
        new PixelArray(reconstImage,width,height);

        System.out.println("Initializing decompression - DeQualitization & Inverse DCT");
        counter = 0;

        for (i = 0; i < width/BlocSize; i++) {
            for (j = 0; j < height/BlocSize; j++) {

                xpos = i * BlocSize;
                ypos = j * BlocSize;

                for (a = 0; a < BlocSize; a++) {
                    for (b = 0; b < BlocSize; b++) {
                        dctArray2[a][b] = reconstImage[xpos + a][ypos + b];
                    }
                }
                int[][] quantum=CompressionTools.initQuantum(Quality,BlocSize);
                dctArray3 = CompressionTools.dequantitizeImage(dctArray2, quantum, BlocSize);


                dctArray4 = dctTrans.fast_idct(dctArray2);


                for (a = 0; a < BlocSize; a++) {
                    for (b = 0; b < BlocSize; b++) {
						// remplir la nouvelle image reconstruite a partir du bloc 8x8 (dctArray4)
                        reconstImage[xpos + a][ypos + b] =(int) dctArray4[a][b];
                    }
                }

            }
        }
        new PixelArray(reconstImage,width,height);
        System.out.println();
        System.out.println("Constructing the images..");
        makeImage(reconstImage);


        System.out.println("Hit enter");
        try {
            System.in.read();
        } catch (Exception e) {
        }
        System.exit(0);

    }
    public void makeImage(int[][] image) {
        int i;
        int j;
        int k = 0;

        int one[] = new int[width * height];

        System.out.println("Calling conversion");
        one = GT.convertGrayToArray(image, true);
        System.out.println();
        System.out.println("Completed one dimensional array conversion");
    }

    public void encodingDecoding(String sourceFile, String outputFile, float quality) {
        try {
            File input = new File(sourceFile);
            BufferedImage image = ImageIO.read(input);

            File compressedImageFile = new File(outputFile);
            OutputStream os = new FileOutputStream(compressedImageFile);

            Iterator<ImageWriter> writers = ImageIO.getImageWritersByFormatName("jpg");
            PixelArray pixelArray = new PixelArray(image);
            GrafixTools grafixTools = new GrafixTools(pixelArray);
            grafixTools.convertRGBtoY(red,green,blue);
            grafixTools.convertRGBtoCb(red,green,blue);
            grafixTools.convertRGBtoCr(red,green,blue);
            ImageWriter writer = writers.next();
            dctTrans = new DCT();
            ImageOutputStream ios = ImageIO.createImageOutputStream(os);
            writer.setOutput(ios);

            ImageWriteParam param = writer.getDefaultWriteParam();

            param.setCompressionMode(2);
            param.setCompressionQuality(quality);
            writer.write(null, new IIOImage(image, null, null), param);
            os.close();
            ios.close();
            writer.dispose();
        } catch (Exception e){
            System.out.println("Sorry! Some error in application. Try again, please!");
        }
    }
}

