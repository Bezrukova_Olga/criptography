package jpegCompression;

/**
 * �����, ����������� ���������� ���������� �������������� (�������� ��� ����� 8 �� 8)
 */


public class DCT
{

  public static double c[][] = new double[8][8];


  public static double cT[][] = new double[8][8];

  public DCT()
  {
    initMatrix();
  }


  private void initMatrix()
  {
    for (int j = 0; j < 8; j++)
      {
        double nn = (double) (8);
        c[0][j] = 1.0 / Math.sqrt(nn);
        cT[j][0] = c[0][j];
      }
    for (int i = 1; i < 8; i++)
      {
      for (int j = 0; j < 8; j++)
        {
          double jj = (double) j;
          double ii = (double) i;
          c[i][j] =
            Math.sqrt(2.0 / 8.0)
            * Math.cos(((2.0 * jj + 1.0) * ii * Math.PI) / (2.0 * 8.0));
          cT[j][i] = c[i][j];
        }
      }
  }



  public static float[][] fast_fdct(float[][] input)
  {
    float output[][] = new float[8][8];
    double temp[][] = new double[8][8];
    double temp1;
    int i;
    int j;
    int k;

    for (i = 0; i < 8; i++)
      {
        for (j = 0; j < 8; j++)
          {
            temp[i][j] = 0.0;
            for (k = 0; k < 8; k++)
              {
                temp[i][j] += (((int) (input[i][k]) - 128) * cT[k][j]);
              }
          }
      }

    for (i = 0; i < 8; i++)
      {
        for (j = 0; j < 8; j++)
          {
            temp1 = 0.0;

            for (k = 0; k < 8; k++)
              {
                temp1 += (c[i][k] * temp[k][j]);
              }

            output[i][j] = (int) Math.round(temp1) * 8;
          }
      }

    return output;
  }

  public double[][] fast_idct(float[][] dctArray2)
  {
    double output[][] = new double[8][8];
    double temp[][] = new double[8][8];
    double temp1;
    int i, j, k;
    for (i = 0; i < 8; i++)
      {
        for (j = 0; j < 8; j++)
          {
            temp[i][j] = 0.0;
            for (k = 0; k < 8; k++)
              {
                temp[i][j] += dctArray2[i][k] * c[k][j];
              }
          }
      }
    for (i = 0; i < 8; i++)
      {
        for (j = 0; j < 8; j++)
          {
            temp1 = 0.0;
            for (k = 0; k < 8; k++)
              temp1 += cT[i][k] * temp[k][j];
            temp1 += 128.0;
            if (temp1 < 0)
              output[i][j] = 0;
            else if (temp1 > 255)
              output[i][j] = 255;
            else
              output[i][j] = (int) Math.round(temp1);
          }
      }
    return output;
  }



}