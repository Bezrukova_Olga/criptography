package jpegCompression;

import javax.swing.*;
import java.awt.image.BufferedImage;


/**
 * � ������ ������ ����������� ������ ��� ��������� �������� �� �������
 */

public class PixelArray{

	private static final long serialVersionUID = 1L;
	private JScrollPane scrollTable=null;
	private JTable table=null;
	private JPanel panelTable=null;
	int[][] pixelArray=null;
	private String[][] pixelArrayStr=null;
	private String[] titles=null;
	private int width;
	private int height;
	private BufferedImage img;
	private Object[][] pixelObject;

	public PixelArray(BufferedImage img){
		super();
		this.img = img;
	}


	public PixelArray(String[][] pixelArrayStr,int width,int height){

		super();
		this.width=width;
		this.height=height;
		this.pixelArrayStr=pixelArrayStr;

		init();
	}

	public PixelArray(int[][] pixelArray,int width,int height){
		super();
		this.width=width;
		this.height=height;
		this.pixelArray=pixelArray;

		init();
	}


	private void init() {

		if(pixelArray!=null)
			pixelObject=convertArray2Object(pixelArray,null,width,height);
		if(pixelArrayStr!=null)
			pixelObject=convertArray2Object(null,pixelArrayStr,width,height);
		titles=new String[width];
		for(int i=0;i<width;i++)
			titles[i]=""+i;

		table=new JTable(pixelObject,titles);
		panelTable=new JPanel();
		panelTable.add(table);
		scrollTable=new JScrollPane(panelTable);
		scrollTable.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollTable.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

	}


	private Object[][] convertArray2Object(int[][] array,String[][] arrayStr,int width,int height) { // �������������� ������ � ������
		Object[][] object=new Object[width+100][height+100];

		for(int i=0;i<width;i++){
			for(int j=0;j<height;j++){
				if(array!=null)
					object[i][j]=new Integer(array[i][j]);
				else if(arrayStr!=null)
					object[i][j]=new String(arrayStr[i][j]);
			}
		}
		return object;
	}
}
