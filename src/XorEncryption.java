import java.util.Scanner;

public class XorEncryption {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter string: ");
        String sampleString = scanner.nextLine(); // вводим строку
        System.out.println("What do you want? - (1) - encrypt, (2) - decrypt");
        String operation = scanner.nextLine(); // выбираем операцию
        System.out.println("Enter cipher: ");
        String cipher = scanner.nextLine(); // вводим символ-шифр
        if (cipher.length() == 1) {
            if (operation.equals("1")) {
                System.out.print("Encrypted string: ");
                encryptDecrypt(sampleString, cipher.charAt(0)); // шифруем, если код операции 1
            } else if (operation.equals("2")) {
                System.out.print("Decrypted string: ");
                encryptDecrypt(sampleString, cipher.charAt(0)); // расшифрорываем, если код операции 2
            } else {
                System.out.println("Invalid operation");
            }
        } else {
            System.out.println("Cipher must be character");
        }
    }


    private static void encryptDecrypt(String inputString, char cipher) { // сама функция кодирования/раскодирования (взаимно обратные операции)
        String outputString = "";
        int len = inputString.length();

        for (int i = 0; i < len; i++) {
            outputString += (char) (inputString.charAt(i) ^ cipher); // делаем исключающее или
        }

        System.out.println(outputString); // выводим результат
    }

}
