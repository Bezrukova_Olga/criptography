import java.util.*;

public class Main {

    public static void main(String[] args) {
        List<Person> persons = new ArrayList<>();
        persons.add(new Person("admin", "admin", Arrays.asList(Grants.values())));
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Выберите действие (1) - войти в систему, (2) - зарегистироваться");
            String operation = scanner.nextLine();
            if (operation.equals("1")) {
                inSystem(persons);
            } else if (operation.equals("2")) {
                System.out.println("Введите логин: ");
                String login = scanner.nextLine();
                while (!uniqueLogin(login, persons)) {
                    System.out.println("Введенный логин уже существует, придумайте новый: ");
                    login = scanner.nextLine();
                }
                System.out.println("Введите пароль: ");
                String password = scanner.nextLine();
                while (!uniquePassword(password, persons)) {
                    System.out.println("Введенный пароль уже существует, придумайте новый: ");
                    password = scanner.nextLine();
                }
                persons.add(new Person(login, password, new ArrayList<>()));
                System.out.println("Вы успешно зарегистированы! Водите в систему");
            } else {
                System.out.println("Введите одну из предложенных операций");
//                Grants.
            }
        }
    }

    private static boolean uniquePassword(String password, List<Person> persons) {
        for (Person person : persons) {
            if (person.getPassword().equals(password))
                return false;
        }
        return true;
    }

    private static boolean uniqueLogin(String login, List<Person> persons) {
        for (Person person : persons) {
            if (person.getLogin().equals(login))
                return false;
        }
        return true;
    }

    private static boolean containsPersonLogin(String login, List<Person> persons) {
        for (Person person : persons) {
            if (person.getLogin().equals(login)) {
                return true;
            }
        }
        return false;
    }

    private static Person findPersonWithLogin(String login, List<Person> persons) {
        for (Person person : persons) {
            if (person.getLogin().equals(login)) {
                return person;
            }
        }
        return null;
    }

    private static void inSystem(List<Person> persons) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите логин: ");
        String login = scanner.nextLine();
        System.out.println("Введите пароль: ");
        String password = scanner.nextLine();
        if (containsPersonLogin(login, persons)) {
            Person person = findPersonWithLogin(login, persons);
            while (!person.getPassword().equals(password)) {
                System.out.println("Неверный пароль, повторите ввод: ");
                password = scanner.nextLine();
            }
            System.out.println("Добро пожаловать! Вам доступны следующие режимы шифрования: ");
            for (int i = 0; i < person.getGrants().size(); i++) {
                System.out.println(i + 1 + ") " + person.getGrants().get(i));
            }
            System.out.println("Выберите один из них или введите exit");
            String cypherCode = scanner.nextLine();
            while (cypherCode.equals("exit")) {
                switch (cypherCode.toUpperCase()) {
                    case ("CODERBOOK"):

                    case ("GRASSHOPPER"):

                    case ("PERMUTATIONENCRYPT"):

                    case ("XORENCRYPTION"):


                }
                System.out.println();
            }
        } else {
            System.out.println("Такого пользователя нет в системе. Выберите регистрацию для создания пользователя");
        }


    }

    private static void addGrands(Map<String, List<Grants>> grants, String currentUser, String nameToAddGrants) {
        Scanner scanner = new Scanner(System.in);
        if (currentUser.equals("admin")) {
            if (grants.containsKey(nameToAddGrants)) {
                System.out.println("Какую привилегию хотите добавить? Для выхода введите exit");
                String privilegious = scanner.nextLine();
                while (!privilegious.equals("exit")) {
                    grants.get(nameToAddGrants).add(Grants.valueOf(privilegious));
                    System.out.println("Какую привилегию хотите добавить? Для выхода введите exit");
                    privilegious = scanner.nextLine();
                }
            } else {
                grants.put(nameToAddGrants, new ArrayList<>());
                System.out.println("Какую привилегию хотите добавить? Для выхода введите exit");
                String privilegious = scanner.nextLine();
                while (!privilegious.equals("exit")) {
                    try {
                        if (Grants.valueOf(privilegious) != null) {
                            grants.get(nameToAddGrants).add(Grants.valueOf(privilegious));
                            System.out.println("Какую привилегию хотите добавить? Для выхода введите exit");
                            privilegious = scanner.nextLine();
                        }
                    } catch (Exception e) {
                        System.out.println("Данная привилегия не найдена. Повторите снова");
                    }
                }
            }
        } else {
            System.out.println("Только администратор может выдавать права");
        }
    }

}
