import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class PermutationEncryption {

    public static void main(String[] args) { // стартовый метод
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите тип операции (1) - кодирование (2) - раскодирование: ");
        String operation = scanner.nextLine(); // ввод типа операции
        if (operation.length() == 1 && (operation.equals("1") || operation.equals("2"))) {
            System.out.println("Кодируемая строка: ");
            String s = scanner.nextLine(); // ввод строки

            int sizeSlb = (int) Math.sqrt(s.length()); // находим необходимое число столбцов
            int sizeStr = (int) Math.ceil((double) s.length() / sizeSlb);// находим необходимое число строк
            if (operation.equals("1")) {
                if ((double) s.length() % sizeSlb != 0) {
                    while ((double) s.length() % sizeSlb != 0) {
                        s += " ";
                    }
                }
                System.out.println("Ключ размерности " + sizeStr + ": ");
                String w = scanner.nextLine();
                int[] symbols = getSymbols(w); // получаем массив значений в ключе
                System.out.println("Ключ размерности " + sizeSlb + ": ");
                String w1 = scanner.nextLine();
                int[] cipher = getSymbols(w1); // получаем массив значений в ключе
                try {
                    if (symbols.length == sizeStr && maxValue(symbols) && repeat(symbols) && cipher.length == sizeSlb && maxValue(cipher) && repeat(cipher)) {
                        String coder = coder(s, symbols, cipher, sizeStr, sizeSlb); // непосредственно кодирование
                        System.out.println("Закодированная строка: " + coder);
                    } else {
                        System.out.println("Error! Incorrect value.");
                    }
                } catch (NullPointerException e) {
                    System.out.println("Ошибка ввода");
                }
            } else if (operation.equals("2") && sizeSlb*sizeStr == s.length()) { // раскодирование строки
                System.out.println("Введите шифр строк " + sizeStr + ": ");
                String str = scanner.nextLine(); // вводим шифр строк
                int[] symbols = getSymbols(str);
                System.out.println("Введите шифр столбцов " + sizeSlb + ": ");
                String str1 = scanner.nextLine(); // вводим шифр столбцов
                int[] symbols1 = getSymbols(str1);
                if (symbols.length == sizeStr && symbols1.length == sizeSlb && maxValue(symbols) && maxValue(symbols1) && repeat(symbols) && repeat(symbols1)) {
                    System.out.println("Расшифрованная строка: " + decoder(s, getSymbols(str), getSymbols(str1), sizeStr, sizeSlb)); // раскодирование строки
                } else {
                    System.out.println("Вы ввели неверный шифр.");
                }
            } else {
                System.out.println("Error");
            }
        } else {
            System.out.println("Повторите ввод");
        }
    }

    private static int[] getSymbols(String line) { // метод для получения символов
        String line1 = line.replaceAll("[\\s]{2,}", " ");
        String[] s = line1.split(" ");
        try {
            int[] array = new int[s.length];
            for (int i = 0; i < s.length; i++) {
                array[i] = Integer.parseInt(s[i]);
            }
            return array;
        } catch (NumberFormatException e) {
            System.out.println("Ошибка ввода");
            return null;
        }
    }

    private static boolean maxValue(int[] array) { // поиск максимального значения
        int max = -1;
        for (int i = 0; i < array.length; i++) {
            if (!(array[i] < 100 && array[i] > 0))
                return false;
        }
        for (int i = 0; i < array.length; i++) {
            if (array[i] > max)
                max = array[i];
        }
        if (array.length != max) {
            return false;
        } else {
            return true;
        }
    }

    private static boolean repeat(int[] array) { // проверка на повторы (для валидации)
        Integer[] arrayInt = new Integer[array.length];
        for (int i = 0; i < array.length; i++) {
            arrayInt[i] = new Integer(array[i]);
        }
        Set<Integer> unique = new HashSet<Integer>(Arrays.asList(arrayInt));
        if (unique.size() == array.length) {
            return true;
        } else {
            return false;
        }
    }

    private static String[] getSymbolsString(String line) { // получить символы в строке
        String[] array = new String[line.length()];
        for (int i = 0; i < line.length(); i++) {
            array[i] = Character.toString(line.charAt(i));
        }
        return array;
    }

    private static String coder(String s, int[] cipher, int[] cipherStlb, int str, int slb) { // метод для кодирования входной строки
        String[][] coder = new String[str][slb];
        int j = 0;
        int k = slb;
        for (int i = 0; i < cipher.length; i++) {
            coder[cipher[i] - 1] = getSymbolsString(s.substring(j, k));
            j += slb;
            k += slb;
        }
        for (int i = 0; i < str; i++) {
            for (int l = 0; l < slb; l++) {
                System.out.print(coder[i][l] + " ");
            }
            System.out.println();
        }
        String result = "";
        for (int i = 0; i < cipherStlb.length; i++) {
            for (int l = 0; l < str; l++) {
                result += coder[l][cipherStlb[i] - 1];
            }
        }
        System.out.println("Result: " + result);
        return result;
    }

    private static String decoder(String s, int[] cipher, int[] cipherStlb, int str, int slb) { // метод для раскодирования входной строки
        String[][] decoder = new String[str][slb];
        int j = 0;
        for (int i = 0; i < cipherStlb.length; i++) {
            for (int l = 0; l < str; l++) {
                decoder[l][cipherStlb[i] - 1] = Character.toString(s.charAt(j));
                j++;
            }
        }
        for (int i = 0; i < str; i++) {
            for (int l = 0; l < slb; l++) {
                System.out.print(decoder[i][l] + " ");
            }
            System.out.println();
        }
        String result = "";
        for (int i = 0; i < cipher.length; i++) {
            for (int k = 0; k < slb; k++) {
                result += decoder[cipher[i] - 1][k];
            }

        }
        System.out.println("Result: " + result);
        return result;
    }

}
