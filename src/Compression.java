import jpegCompression.JEncoder;

import java.io.IOException;
import java.util.Scanner;

public class Compression {
    public static void main(String[] args) throws IOException {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter operation (1)-compress, (2)-decompress");
        String operation = scanner.nextLine();

        try{
        if (operation.equals("1")) {
            JEncoder jEncoder = new JEncoder(); // класс, в котором реализована логика сжатия
            jEncoder.encodingDecoding("smile.jpg", "compress.jpg", 0.06f); // передаем значения в метод для кодирования
        } else if (operation.equals("2")) {
            JEncoder jEncoder = new JEncoder();
            jEncoder.encodingDecoding("smile.jpg", "decompress.jpg", 0.2f); // передаем значения в метод для раскодирования
        } else {
            System.out.println("You enter error operation! Try again!");
        }}catch(Exception e){
            System.out.println("Error");
        }
    }



}
