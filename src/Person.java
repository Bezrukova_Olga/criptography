import java.util.List;

public class Person {
    private String login;
    private String password;
    private List<Grants> grants;

    public Person(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public Person(String login, String password, List<Grants> grants) {
        this.login = login;
        this.password = password;
        this.grants = grants;
    }

    public List<Grants> getGrants() {
        return grants;
    }

    public void setGrants(List<Grants> grants) {
        this.grants = grants;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
