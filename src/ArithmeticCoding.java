import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class ArithmeticCoding {
    public static HashMap<Character, Double> rangeLow = new HashMap<Character, Double>();
    public static HashMap<Character, Double> rangeHigh = new HashMap<Character, Double>();
    public static char lastChar = ' ';
    public static Double prefix = 0.0000000001; // небольшое число, при котором почему-то повышается точность шифрования
    public static Map<Character, Double> prob = new HashMap<>();

    public static Double compress(String s) { // метод сжатия
        Double Lower = 0.0, Upper = 1.0, originLower;
        for (int i = 0; i < s.length(); ++i) {
            originLower = Lower;
            Lower = originLower + (Upper - originLower) * rangeLow.get(s.charAt(i));
            Upper = originLower + (Upper - originLower) * rangeHigh.get(s.charAt(i));
        }
        return nearest(Lower, Upper);
    }

    public static String decompress(Double code, int numSym) { // расшифровка
        double Lower = 0.0, Upper = 1.0, newCode = code, originLower;
        String s = "";
        for (int i = 0; i < numSym; ++i) {
            for (Character c : rangeLow.keySet()) {
                if (newCode > rangeLow.get(c) && newCode < rangeHigh.get(c)) {
                    s += c;
                    originLower = Lower;
                    Lower = originLower + (Upper - originLower) * rangeLow.get(c);
                    Upper = originLower + (Upper - originLower) * rangeHigh.get(c);
                    newCode = (code - Lower) / (Upper - Lower);
                    break;
                }
            }
        }
        return s;
    }

    public static Double nearest(Double low, Double high) { // метод для определения близости
        Double num = 0.0;
        Double i = -1.0;
        while (!(num > low && num < high) && i > -1000.0) {
            num += Math.pow(2, i);
            if (num > high) {
                num -= Math.pow(2, i);
            }
            --i;
        }
        return num;
    }

    public static void addRange(Character c, Double range) { // составление таблицы диапазонов
        if (rangeLow.size() == 0) {
            rangeLow.put(c, 0.0);
            rangeHigh.put(c, range);
        } else {
            rangeLow.put(c, rangeHigh.get(lastChar));
            rangeHigh.put(c, rangeHigh.get(lastChar) + range);
        }
        lastChar = c;
    }

    public static void probability(String inputText) { // рассчет вероятности появления буквы
        for (int i = 0; i < inputText.length(); i++) {
            char symbol = inputText.charAt(i);
            if (prob.containsKey(symbol)) {
                prob.replace(symbol, prob.get(symbol) + 1);
            } else {
                prob.put(symbol, 1.0);
            }
        }
        for (Map.Entry<Character, Double> entry : prob.entrySet()) {
            prob.replace(entry.getKey(), entry.getValue() / inputText.length());
            addRange(entry.getKey(), entry.getValue());
        }
        System.out.println(prob.toString());
    }

    public static void main(String[] args) {

        try {
            String dataWord = "";
            String data = "";
            Scanner scan = new Scanner(System.in);
            System.out.println("Enter operation (1)-encode, (2)-decode");
            String operation = scan.nextLine(); // ввод операции
            if (operation.equals("1")) {
                try {
                    File myObj = new File("text.txt"); // считывание входной строки из файла
                    Scanner myReader = new Scanner(myObj);
                    dataWord = myReader.nextLine();
                    myReader.close();
                    probability(dataWord); // расчет вероятности появления буквы
                    System.out.println("Compress: " + compress(dataWord)); // сжатие
                } catch (FileNotFoundException e) {
                    System.out.println("An error occurred.");
                }
            } else if (operation.equals("2")) {
                try {
                    File myObj = new File("text.txt");
                    Scanner myReader = new Scanner(myObj);
                    dataWord = myReader.nextLine(); // считывание строки
                    myReader.close();
                    File prob = new File("probability.txt");
                    Scanner scanner = new Scanner(prob);
                    data = scanner.nextLine(); // считывание вероятности появления буквы, введенной пользователем
                    while (scanner.hasNextLine()) {
                        String line = scanner.nextLine();
                        String[] result = line.split(" ");
                        addRange(result[0].charAt(0), Double.parseDouble(result[1])+prefix);
                    }
                    scanner.close();
                    System.out.println("Decompress: "+decompress(Double.parseDouble(dataWord), Integer.parseInt(data))); // декодирование
                } catch (FileNotFoundException e) {
                    System.out.println("An error occurred.");
                }
            } else {
                System.out.println("Incorrect input string. Try again!");
            }
        } catch (Exception e){
            System.out.println("Sorry! We have some error. Please, check your input data. If all are good write message in our support group.");
        }
    }
}